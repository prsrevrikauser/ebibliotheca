Rails.application.routes.draw do
  root 'books#index'
  
  get 'books/all' => 'books#all', as: 'all_books'
  get 'search' => 'books#search'
  get 'admin' => 'sessions#new', as: 'admin'
  get 'return' => 'users#return', as: 'return_book'

  get 'books/manage' => 'books#manage', as: 'manage_books'
  get 'books/list' => 'books#list', as: 'list_books'
  get 'orders/manage' => 'orders#manage', as: 'manage_orders'
  get 'orders/history' => 'orders#history', as: 'order_history'
  get 'rentals/manage' => 'rentals#manage', as: 'manage_rentals'
  get 'rentals/history' => 'rentals#history', as: 'rental_history'
  put 'orders/manage/:id/moderate' => 'orders#moderate', as: 'moderate_order'
  put 'orders/manage/:id/complete' => 'orders#complete', as: 'complete_order'

  resources :books do
    resources :rentals
  end

  resources :users

  resource :session, only: [:new, :create, :destroy]

  resources :orders
  resources :categories

end
