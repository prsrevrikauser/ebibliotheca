class Book < ApplicationRecord
  has_many :rentals, dependent: :destroy
  has_many :book_quantities, dependent: :destroy
  has_many :categorizations, dependent: :destroy
  has_many :categories, through: :categorizations

  # validations
  validates :name,
    presence: true,
    length: { maximum: 250 }
  
  validates :author,
    length: {
      minimum: 5,
      maximum: 200
    }
  
  validates :description,
    length: { minimum: 100 }
  
  validates :image_src, 
    presence: true, 
    length: { maximum: 250 },
    format: {
      with: /\w+\.(jpg|png|jpeg)\z/i,
      message: 'must be a JPG, JPEG, or PNG'
    }
  
  validates :cit_quantity, :ala_quantity,
    numericality: {
      greater_than_or_equal_to: 0,
      only_integer: true,
    }

  # class methods
  def self.newest
    where(visible: true)
      .order(created_at: :desc)
      .limit(20)
  end
  
  def self.search(query)
    q = '%' + query + '%'
    where('name LIKE ?', q)
  end

  def self.all_ordered_by_name
    order('name ASC')
  end

  def self.all_ordered_by_category(category_id)
    if Category.exists?(category_id) 
      Category.find(category_id).books
    else
      all_ordered_by_name
    end
  end

  def self.all_ordered_by_times_read
    order(times_read: :desc)
  end

  # instance methods
  def rented_out?
    cit_quantity.zero? && ala_quantity.zero?
  end

  def has_readers?
    unreturned_rentals.size > 0
  end

  def available?
    available_in_ala? || available_in_cit?
  end

  def available_in_cit?
    cit_quantity > 0
  end

  def available_in_ala?
    ala_quantity > 0
  end

  def unreturned_rentals
    rentals.where(returned: false)
  end

  def returned_rentals
    rentals.where(returned: true)
  end

  def q_cit_quantity
    book_quantities.first.cit
  end

  def q_ala_quantity
    book_quantities.first.ala
  end
  
end
