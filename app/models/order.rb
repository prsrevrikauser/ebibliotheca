class Order < ApplicationRecord
  belongs_to :user

  # validations
  validates :name,
    presence: true,
    length: { maximum: 250 }
  
  validates :author,
    length: {
      minimum: 5,
      maximum: 200
    }

  def self.to_be_ordered
    where(ordered: false, moderated: true)
      .order(created_at: :desc)
  end

  def self.newest
    where(ordered: false)
      .order(created_at: :desc)
  end

  def self.completed
    where(ordered: true)
      .order(created_at: :desc)
  end
end
