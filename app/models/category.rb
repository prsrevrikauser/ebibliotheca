class Category < ApplicationRecord

  has_many :categorizations, dependent: :destroy
  has_many :books, through: :categorizations

  # validations
  validates :name, presence: true, uniqueness: true

end
