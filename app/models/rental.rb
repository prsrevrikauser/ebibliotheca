class Rental < ApplicationRecord
  belongs_to :book
  belongs_to :user

  # validations
  validates :return_date, presence: true

  # validate :return_date_cannot_be_in_the_past,
  #   :rental_days_must_be_more_than_three,
  #   :rental_days_must_be_less_than_21days

  def self.unreturned
    where(returned: false)
      .order(created_at: :desc)
  end

  def self.returned
    where(returned: true)
      .order(created_at: :desc)
  end

  def return_date_cannot_be_in_the_past
    if return_date.present? && return_date < Date.today
      errors.add(:return_date, "can't be in the past")
    end
  end

  def rental_days_must_be_more_than_three
    if return_date.present? && return_date < (Date.today + 3.days)
      errors.add(:return_date, "can't be less than 3 days")
    end
  end

  def rental_days_must_be_less_than_21days
    if return_date.present? && return_date > (Date.today + 20.days)
      errors.add(:return_date, "can't be more than 21 days")
    end
  end

  def city
    if got_from == 'cit'
      'Шымкент'
    else
      'Алматы'
    end
  end
end
