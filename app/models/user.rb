class User < ApplicationRecord
  has_many :rentals, dependent: :destroy
  has_many :orders, dependent: :destroy
  
  has_secure_password

  # validations
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :department, presence: true

  validates :email,
    format: { with: /\S+@\S+/ },
    uniqueness: { case_sensitive: false }

  def full_name
    "#{first_name.capitalize} #{last_name.capitalize}"
  end

  def first_name_and_initials
    "#{first_name.capitalize} #{last_name[0].capitalize}."
  end

  def unreturned_rentals
    rentals.where(returned: false)
  end

  def returned_rentals
    rentals.where(returned: true)
  end

  def admin?
    admin && role == 'admin'
  end

end
