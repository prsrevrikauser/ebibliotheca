class RentalsController < ApplicationController
  before_action :require_signin, only: [:manage, :history]
  before_action :require_admin, only: [:manage, :history]

  before_action :set_event, except: [:manage, :history]

  def history
    @rentals = Rental.returned
  end

  def manage
    @rentals = Rental.unreturned
  end

  def index
    @rentals = @book.unreturned_rentals
  end

  def new
    @rental = @book.rentals.new
  end

  def create
    begin
      email = get_email_from_param(params[:rental][:account])
      user = User.find_by email: email
    rescue ActiveRecord::RecordNotFound => exception
      redirect_to new_book_rental_url(@rental.book), alert: 'Неправильный ID (или почта)'
    end
    
    @rental = @book.rentals.new(rental_params)
    @rental.user = user

    if @rental.save

      if @rental[:got_from] == 'ala'
        @book.ala_quantity -= 1
        @book.save
      end

      if @rental[:got_from] == 'cit'
        @book.cit_quantity -= 1
        @book.save
      end

      redirect_to book_rentals_url(@book),
        notice: 'Книга успешна была присвоена вам :)'
    else
      render :new
    end
  end

  def destroy
    @rental = Rental.find(params[:rental_id])
    
    if @rental.update(returned: true)

      if @rental[:got_from] == 'ala'
        @book.ala_quantity += 1
        @book.times_read += 1
        @book.save
      end

      if @rental[:got_from] == 'cit'
        @book.cit_quantity += 1
        @book.times_read += 1
        @book.save
      end

      redirect_to user_path(@rental.user, account: get_email_from_param(@rental.user.email)),
        notice: 'Вы успешно вернули книгу на полку. '

    end
  end

  private

  def rental_params
    params.require(:rental)
      .permit(:return_date, :got_from)
  end

  def set_event
    @book = Book.find(params[:book_id])
  end

  def get_email_from_param(string)
    return string if string.include?('@')

    "#{string}@evrika.com"
  end

end
