class BooksController < ApplicationController

  before_action :require_signin, except: [:index, :show, :all, :search]
  before_action :require_admin, except: [:index, :show, :all, :search, :manage]

  def all
    @categories = Category.all
    
    if params['category_id']
      @books = Book.all_ordered_by_category(params['category_id'])
    else
      @books = Book.all_ordered_by_name
    end
  end

  def search
    @query = params[:query]
    @books = Book.search(params[:query])
  end

  def manage
  end

  def list
    @books = Book.all_ordered_by_times_read
  end

  def index
    @books = Book.newest
  end

  def show
    @book = Book.find(params[:id])
  end

  def new
    @book = Book.new

    if params[:book_name] && params[:book_author]
      @book.name = params[:book_name]
      @book.author = params[:book_author]
    end
  end

  def create
    @book = Book.new(book_params)

    # add quantities
    q = @book.book_quantities.new
    q.cit, q.ala = @book.cit_quantity, @book.ala_quantity
    
    if @book.save && q.save
      redirect_to book_path(@book), notice: 'Книга была успешна добавлена!'
    else
      render :new
    end
  end

  def edit
    @book = Book.find(params[:id])
  end

  def update
    @book = Book.find(params[:id])

    # update quantities
    q = @book.book_quantities.first
    q.cit, q.ala = book_params[:cit_quantity], book_params[:ala_quantity]
    
    if @book.update(book_params) && q.save
      redirect_to book_path(@book), notice: 'Книга была успешна обновлена!'
    else
      render :edit
    end
  end

  # toggle effect
  def destroy
    @book = Book.find(params[:id])

    if @book.visible?
      @book.visible = false
      flash[:notice] = 'Успех. Пользователи теперь не увидят эту книгу.'
    else
      @book.visible = true
      flash[:notice] = 'Успех. Пользователи теперь могут увидеть эту книгу.'
    end

    # @book.visible = @book.visible? ? false : true
    @book.save

    if params[:src] && params[:src] == 'dashboard'
      redirect_to list_books_path
    else
      redirect_to book_path(@book)
    end

  end

  private

  def book_params
    params.require(:book)
      .permit(:name, :author, :description, :ala_quantity, :cit_quantity, :image_src, :visible, category_ids: [])
  end

end
