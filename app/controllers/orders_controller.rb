class OrdersController < ApplicationController

  before_action :require_signin, only: [:manage, :moderate, :complete, :history]
  before_action :require_admin, only: [:manage, :moderate, :complete, :history]

  def manage
    @orders = Order.newest
  end

  def moderate
    @order = Order.find(params[:id])
    
    # if @order.moderated?
    #   @order.moderated = false
    # else
    #   @order.moderated = true
    # end

    @order.moderated = @order.moderated? ? false : true
    @order.save

    redirect_to manage_orders_url,
      notice: 'Статус модерация заказа успешно обновлён!'
  end

  def complete
    @order = Order.find(params[:id])
    
    @order.ordered = true
    @order.save

    redirect_to new_book_url(
      book_name: @order.name,
      book_author: @order.author
    )
  end

  def history
    @orders = Order.completed
  end

  def index
    @orders = Order.to_be_ordered
  end

  def new
    account = get_email_from_param(params[:account])
    @user = User.find_by email: account

    if @user.nil?
      redirect_to orders_url, alert: 'Неправильный ID (или почта)'
    end

    @order = Order.new
  end

  def create
    @order = Order.new(order_params)
    
    if @order.save
      redirect_to orders_url, notice: 'Твой заказ в обработке. Как только он пройдёт модерацию, ты увидишь его в списке желаемой литературы :)'
    else
      render :new
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.delete

    redirect_to manage_orders_url,
      notice: 'Заказ был успешно удалён из базы!'
  end

  private

  def get_email_from_param(string)
    return string if string.include?('@')

    "#{string}@evrika.com"
  end

  def order_params
    params.require(:order)
      .permit(:name, :author, :link, :user_id)
  end

end
