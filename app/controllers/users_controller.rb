class UsersController < ApplicationController
  before_action :require_signin, only: [:index, :create]
  before_action :require_admin, only: [:new, :create]

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    
    @user.password = "0000"
    @user.password_confirmation = "0000"

    if @user.save
      redirect_to manage_books_path, notice: 'Пользователь был успешно добавлен!'
    else
      render :new
    end
  end

  def show
    email = get_email_from_param(params[:account])
    @user = User.find_by email: email
    
    if @user.nil?
      redirect_to return_book_url, alert: 'Неправильный ID (или почта)'
    end
  end

  def return
  end

  private

  def get_email_from_param(string)
    return string if string.include?('@')

    "#{string}@evrika.com"
  end

  def user_params
    params.require(:user)
      .permit(:first_name, :last_name, :email, :department)
  end

end
