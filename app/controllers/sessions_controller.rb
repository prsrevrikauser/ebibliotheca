class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    
    if user && user.admin? && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to manage_books_path, notice: "Добро пожаловать, #{user.first_name}!"
    else
      flash.now[:alert] = 'Неправильная электронная почта и (или) пароль :('
      render :new
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: 'Вы успешно вышли из аккаунта)'
  end

end
