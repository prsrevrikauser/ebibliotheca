module RentalsHelper

  def rented_out_text(book)
    if book.rented_out?
      return content_tag(:p, 'На данный момент книги нет в наличии ;( Посмотрите пожалуйста список читающих и попросите вернуть пораньше.', class: tw(:rented_out_text))
    end

    nil
  end

  def get_from_ala(book)
    if book && book.visible? && book.available_in_ala?
      return link_to('Взять из Алматы',
        new_book_rental_path(book, city: 'ala'),
        class: tw(:add_book_rental_btn))
    end

    nil
  end

  def get_from_cit(book)
    if book && book.visible? && book.available_in_cit?
      return link_to('Взять из Шымкента',
        new_book_rental_path(book, city: 'cit'),
        class: tw(:add_book_rental_btn))
    end

    nil
  end

  def readers_list(book)
    return nil unless book.has_readers?

    link_to 'Список читающих', book_rentals_path(@book),  class: tw(:book_rentals_btn)
  end

end
