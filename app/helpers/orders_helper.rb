module OrdersHelper

  def moderate_link(order)
    flag = order.moderated? ? '🟢' : '🔴'
    
    link_to flag,
      moderate_order_path(order),
      method: :put,
      class: 'mx-1',
      data: {
        confirm: 'Вы уверены, что хотите изменить модерацию этого заказа?'
      }
  end

  def order_done_link(order)
    link_to '☑️',
      complete_order_path(order),
      method: :put,
      class: 'mx-1',
      data: {
        confirm: 'Вы уверены, что хотите изменить статус заказа на "выпонено"?'
      }
  end

  def delete_order_link(order)
    link_to '❌',
      order_path(order),
      method: :delete,
      class: 'mx-1',
      data: {
        confirm: 'Вы уверены, что хотите полностью удалить данный заказ из быза?'
      }
  end

end
