module ApplicationHelper
  
  # view classes for cleanliness
  TAILWIND_CLASSES = {
    add_book_btn: 'text-lg font-bold bg-blue-500 text-white px-6 py-2 inline-block mr-6 my-1',

    book_card_title: 'text-blue-500 text-2xl hover:underline hover:text-blue-500',

    edit_book_btn: 'text-lg font-bold bg-blue-500 text-white px-6 py-2 inline-block mr-4',

    form_input: 'focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus:outline-none text-base text-black placeholder-gray-500 border-2 border-blue-200 py-2 pl-4 w-96',

    form_text_area: 'focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus:outline-none text-base text-black placeholder-gray-500 border-2 border-blue-200 py-2 pl-4 w-full',

    hide_book_btn: 'text-lg font-bold bg-red-500 text-white px-6 py-2 inline-block mr-4',
    
    show_book_btn: 'text-lg font-bold bg-green-500 text-white px-6 py-2 inline-block mr-4',

    rental_list: 'my-2 pb-1 list-disc list-inside',

    book_rentals_btn: 'text-lg font-bold bg-purple-500 text-white px-6 py-2 inline-block mr-4 mt-2',

    add_book_rental_btn: 'text-lg font-bold bg-green-500 text-white px-6 py-2 inline-block mr-4',

    rental_list_book_title: 'font-bold text-blue-500 hover:underline hover:text-blue-500',

    rental_form: 'flex flex-col bg-green-50 py-5 px-6 mt-6',

    errors_list: 'mb-4 p-3 text-red-500 bg-yellow-200 bg-red-500 list-decimal list-inside',

    nav_item: 'block w-28 px-4 py-2 bg-blue-500 hover:bg-blue-500 text-white text-center text-lg capitalize',

    logo: 'text-lg font-bold uppercase text-yellow-600 text-center block tracking-widest mt-1',

    search_input: 'focus:border-blue-500 focus:ring-1 focus:ring-blue-500 focus:outline-none w-full text-base text-black placeholder-gray-500 border-2 border-blue-200 py-2 pl-4',

    search_input_submit: 'text-white bg-yellow-600 hover:bg-blue-600 ml-2 px-2 cursor-pointer text-lg',

    rented_out_text: 'py-2 px-3 mb-2 border-2 border-solid border-yellow-500',

    return_form: 'flex flex-col bg-green-50 py-5 px-6 mt-6',

    return_book_btn: 'py-1 px-2 bg-blue-500 text-white',

    category_button: 'border-2 border-white bg-yellow-600 py-1 px-2 my-1 mx-2 text-white'
  }

  def tw(class_name)
    if TAILWIND_CLASSES.key?(class_name)
      return TAILWIND_CLASSES[class_name]
    end

    nil
  end

end
