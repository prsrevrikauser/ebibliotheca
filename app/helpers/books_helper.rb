module BooksHelper

  def cover(book)
    image_tag book.image_src, 
      alt: "Обложка книги #{book.name}",
      class: 'absolute inset-0 w-full h-full object-cover rounded-lg'
  end

  def quantity(book)
    unless book.available?
      return content_tag(:p, 'книга не доступна ;(')
    end

    qty = []

    if book.available_in_cit?
      qty << "Шымкент (#{book.cit_quantity})"
    end

    if book.available_in_ala?
      qty << "Алматы (#{book.ala_quantity})"
    end

    content_tag(:p, qty.join(', '))
  end

  def availability_in_cities(book)
  end

  def hide_or_show(book)
    if book.visible?
      return link_to 'Скрыть',
        book_path(@book),
        class: tw(:hide_book_btn),
        method: :delete,
        data: { confirm: 'Вы уверены, что хотите скрыть книгу?' }
    end

    link_to 'Показать',
      book_path(@book),
      class: tw(:show_book_btn),
      method: :delete,
      data: { confirm: 'Вы уверены, что хотите сделать книгу видимой?' }
  end

  def edit_link(book)
    link_to '📝',
      edit_book_path(book), 
      class: 'mx-1', 
      title: 'Редактировать'
  end

  def toggle_book_visibility(book)
    flag = book.visible? ? '🟢' : '🔴'
    flag_message = book.visible? ? 'скрыть' : 'показать'
    
    link_to flag,
      book_path(book, src: 'dashboard'),
      method: :delete,
      class: 'mx-1',
      data: {
        confirm: "Вы уверены, что хотите #{flag_message} эту книгу?"
      }
  end

end
