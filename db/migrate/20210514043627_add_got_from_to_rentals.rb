class AddGotFromToRentals < ActiveRecord::Migration[6.1]
  def change
    add_column :rentals, :got_from, :string
  end
end
