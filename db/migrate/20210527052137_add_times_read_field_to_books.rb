class AddTimesReadFieldToBooks < ActiveRecord::Migration[6.1]
  def change
    add_column :books, :times_read, :integer, default: 0
  end
end
