class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.string :name
      t.string :author
      t.string :link
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
