class CreateBookQuantities < ActiveRecord::Migration[6.1]
  def change
    create_table :book_quantities do |t|
      t.integer :cit, default: 0
      t.integer :ala, default: 0
      t.references :book, null: false, foreign_key: true

      t.timestamps
    end
  end
end
