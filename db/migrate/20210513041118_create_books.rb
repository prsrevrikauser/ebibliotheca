class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.string :name
      t.string :author
      t.string :description
      t.string :image_src, default: 'placeholder_cover.jpg'
      t.integer :ala_quantity, default: 0
      t.integer :cit_quantity, default: 0
      t.boolean :visible, default: true

      t.timestamps
    end
  end
end
