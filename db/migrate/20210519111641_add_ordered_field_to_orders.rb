class AddOrderedFieldToOrders < ActiveRecord::Migration[6.1]
  def change
    add_column :orders, :ordered, :boolean, default: false
  end
end
