class MakeRentalsAJoinTable < ActiveRecord::Migration[6.1]
  def change
    remove_column :rentals, :name, :string
    remove_column :rentals, :email, :string

    add_column :rentals, :user_id, :integer

    Rental.delete_all
  end
end
