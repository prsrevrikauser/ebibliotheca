class CreateRentals < ActiveRecord::Migration[6.1]
  def change
    create_table :rentals do |t|
      t.string :name
      t.string :email
      t.references :book, null: false, foreign_key: true

      t.timestamps
    end
  end
end
