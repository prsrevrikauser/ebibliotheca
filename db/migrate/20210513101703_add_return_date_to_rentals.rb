class AddReturnDateToRentals < ActiveRecord::Migration[6.1]
  def change
    add_column :rentals, :return_date, :date
  end
end
