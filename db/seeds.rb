# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# seedding with CSV
require 'csv'

csv_users = File.read(Rails.root.join('db/office_staff.csv'))
users = CSV.parse(csv_users, headers: true, encoding: 'UTF-8')

users.each do |row|
  User.create(
    first_name: row['first_name'],
    last_name: row['last_name'],
    email: row['email'],
    department: row['department'],
    password: row['password'],
    password_confirmation: row['password_confirmation']
  )
end
